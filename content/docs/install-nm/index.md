---
title: 'Install NetworkManager'
date: 2019-02-11T19:27:37+10:00
weight: 4
summary: How to install NetworkManager
---

NetworkManager is available on almost every major linux distribution and you can install it using your package manager.

#### Fedora

```
$ dnf install -y NetworkManager
```

#### Start NM

After installing you can enable and start the NetworkManager daemon using systemd.
```
$ systemctl --now enable NetworkManager.service
```
